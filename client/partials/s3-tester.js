images = {};

Template.s3_tester.events({
    "click button.upload" : function() {
        var files = $('input.file_bag')[0].files;
        var files_count = files.length;
        var i = 1;
        S3.upload({
            files: files,
            path: 'uploads'
        }, function(err, resp) {
            images[resp._id] =  _.clone(resp.file);
            _.extend(images[resp._id], {url : resp.url});
            if (i === files_count) {
                console.log(images);
            }
            i++;
        });
    }
});

Template.s3_tester.helpers({
    "files": function() {
        return S3.collection.find();
    }
})
