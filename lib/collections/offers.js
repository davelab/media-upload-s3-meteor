OfferSchema = SimpleSchema({
    name: {
        type: String
    },
    description: {
        type: String,
    },
    is_availabe: {
        type: Boolean
    },
    available_start: {
        type: Date
    },
    available_end: {
        type: Date
    }
});
