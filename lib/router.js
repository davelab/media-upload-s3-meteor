Router.configure({
  layoutTemplate: 'frontTemplate',
});

Router.route('home', {
    path: '/'
});

Router.route('admin', {
    path: '/admin',
    layoutTemplate: 'adminTemplate'
})
